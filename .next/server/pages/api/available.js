"use strict";
(() => {
var exports = {};
exports.id = "pages/api/available";
exports.ids = ["pages/api/available"];
exports.modules = {

/***/ "./src/pages/api/available.js":
/*!************************************!*\
  !*** ./src/pages/api/available.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ send)
/* harmony export */ });
async function send(req, res) {
  const {
    query: {
      id
    }
  } = req;
  const domain = "el-dorado-pk.myshopify.com";
  const storefrontAccessToken = "d35cc2760d85327edfc2475b867e4d92";
  console.log("Domain ERT: ", domain);
  console.log("Token: ", storefrontAccessToken);

  async function ShopifyData(query) {
    const URL = `https://${domain}/api/2022-10/graphql.json`;
    const options = {
      endpoint: URL,
      method: "POST",
      headers: {
        "X-Shopify-Storefront-Access-Token": storefrontAccessToken,
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        query
      })
    };

    try {
      const data = await fetch(URL, options).then(response => {
        console.log("Response: ", response);
        return response.json();
      });
      return data;
    } catch (error) {
      console.log("Error: ", error);
      throw new Error("Products not fetched");
    }
  }

  async function getProduct(handle) {
    var _response$data;

    const query = `
    {
      product(handle: "${handle}") {
        id
        variants(first: 25) {
          edges {
            node {
              id
              availableForSale
            }
          }
        }
      }
    }`;
    const response = await ShopifyData(query);
    const product = (_response$data = response.data) !== null && _response$data !== void 0 && _response$data.product ? response.data.product : [];
    console.log("Product: ", product);
    return product;
  }

  const product = await getProduct(id);
  res.json(product);
}

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./src/pages/api/available.js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZXMvYXBpL2F2YWlsYWJsZS5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFlLGVBQWVBLElBQWYsQ0FBb0JDLEdBQXBCLEVBQXlCQyxHQUF6QixFQUE4QjtBQUMzQyxRQUFNO0FBQ0pDLElBQUFBLEtBQUssRUFBRTtBQUFFQyxNQUFBQTtBQUFGO0FBREgsTUFFRkgsR0FGSjtBQUlBLFFBQU1JLE1BQU0sR0FBR0MsNEJBQWY7QUFDQSxRQUFNRyxxQkFBcUIsR0FBR0gsa0NBQTlCO0FBS0RLLEVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGNBQVosRUFBNEJQLE1BQTVCO0FBQ0FNLEVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLFNBQVosRUFBdUJILHFCQUF2Qjs7QUFXQyxpQkFBZUksV0FBZixDQUEyQlYsS0FBM0IsRUFBa0M7QUFDaEMsVUFBTVcsR0FBRyxHQUFJLFdBQVVULE1BQU8sMkJBQTlCO0FBRUEsVUFBTVUsT0FBTyxHQUFHO0FBQ2RDLE1BQUFBLFFBQVEsRUFBRUYsR0FESTtBQUVkRyxNQUFBQSxNQUFNLEVBQUUsTUFGTTtBQUdkQyxNQUFBQSxPQUFPLEVBQUU7QUFDUCw2Q0FBcUNULHFCQUQ5QjtBQUVQLGtCQUFVLGtCQUZIO0FBR1Asd0JBQWdCO0FBSFQsT0FISztBQVFkVSxNQUFBQSxJQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQUVsQixRQUFBQTtBQUFGLE9BQWY7QUFSUSxLQUFoQjs7QUFXQSxRQUFJO0FBQ0YsWUFBTW1CLElBQUksR0FBRyxNQUFNQyxLQUFLLENBQUNULEdBQUQsRUFBTUMsT0FBTixDQUFMLENBQW9CUyxJQUFwQixDQUF5QkMsUUFBUSxJQUFJO0FBQ3REZCxRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxZQUFaLEVBQTBCYSxRQUExQjtBQUNBLGVBQU9BLFFBQVEsQ0FBQ0MsSUFBVCxFQUFQO0FBQ0QsT0FIa0IsQ0FBbkI7QUFLQSxhQUFPSixJQUFQO0FBQ0QsS0FQRCxDQU9FLE9BQU9LLEtBQVAsRUFBYztBQUNkaEIsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUF1QmUsS0FBdkI7QUFDQSxZQUFNLElBQUlDLEtBQUosQ0FBVSxzQkFBVixDQUFOO0FBQ0Q7QUFDRjs7QUFFRCxpQkFBZUMsVUFBZixDQUEwQkMsTUFBMUIsRUFBa0M7QUFBQTs7QUFDaEMsVUFBTTNCLEtBQUssR0FBSTtBQUNuQjtBQUNBLHlCQUF5QjJCLE1BQU87QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQWJJO0FBZUEsVUFBTUwsUUFBUSxHQUFHLE1BQU1aLFdBQVcsQ0FBQ1YsS0FBRCxDQUFsQztBQUVBLFVBQU00QixPQUFPLEdBQUcsa0JBQUFOLFFBQVEsQ0FBQ0gsSUFBVCwwREFBZVMsT0FBZixHQUF5Qk4sUUFBUSxDQUFDSCxJQUFULENBQWNTLE9BQXZDLEdBQWlELEVBQWpFO0FBQ0FwQixJQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxXQUFaLEVBQXlCbUIsT0FBekI7QUFDQSxXQUFPQSxPQUFQO0FBQ0Q7O0FBRUQsUUFBTUEsT0FBTyxHQUFHLE1BQU1GLFVBQVUsQ0FBQ3pCLEVBQUQsQ0FBaEM7QUFDQUYsRUFBQUEsR0FBRyxDQUFDd0IsSUFBSixDQUFTSyxPQUFUO0FBQ0QiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly92aWUtcmVhY3QvLi9zcmMvcGFnZXMvYXBpL2F2YWlsYWJsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBhc3luYyBmdW5jdGlvbiBzZW5kKHJlcSwgcmVzKSB7XHJcbiAgY29uc3Qge1xyXG4gICAgcXVlcnk6IHsgaWQgfSxcclxuICB9ID0gcmVxXHJcblxyXG4gIGNvbnN0IGRvbWFpbiA9IHByb2Nlc3MuZW52LlNIT1BJRllfU1RPUkVfRE9NQUlOXHJcbiAgY29uc3Qgc3RvcmVmcm9udEFjY2Vzc1Rva2VuID0gcHJvY2Vzcy5lbnYuU0hPUElGWV9TVE9SRUZST05UX0FDQ0VTU1RPS0VOXHJcblxyXG5cclxuIFxyXG5cclxuIGNvbnNvbGUubG9nKFwiRG9tYWluIEVSVDogXCIsIGRvbWFpbilcclxuIGNvbnNvbGUubG9nKFwiVG9rZW46IFwiLCBzdG9yZWZyb250QWNjZXNzVG9rZW4pXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG4gIGFzeW5jIGZ1bmN0aW9uIFNob3BpZnlEYXRhKHF1ZXJ5KSB7XHJcbiAgICBjb25zdCBVUkwgPSBgaHR0cHM6Ly8ke2RvbWFpbn0vYXBpLzIwMjItMTAvZ3JhcGhxbC5qc29uYFxyXG5cclxuICAgIGNvbnN0IG9wdGlvbnMgPSB7XHJcbiAgICAgIGVuZHBvaW50OiBVUkwsXHJcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBcIlgtU2hvcGlmeS1TdG9yZWZyb250LUFjY2Vzcy1Ub2tlblwiOiBzdG9yZWZyb250QWNjZXNzVG9rZW4sXHJcbiAgICAgICAgXCJBY2NlcHRcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgIH0sXHJcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHsgcXVlcnkgfSlcclxuICAgIH1cclxuXHJcbiAgICB0cnkge1xyXG4gICAgICBjb25zdCBkYXRhID0gYXdhaXQgZmV0Y2goVVJMLCBvcHRpb25zKS50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIlJlc3BvbnNlOiBcIiwgcmVzcG9uc2UpXHJcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmpzb24oKVxyXG4gICAgICB9KVxyXG5cclxuICAgICAgcmV0dXJuIGRhdGFcclxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3I6IFwiLCBlcnJvcilcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiUHJvZHVjdHMgbm90IGZldGNoZWRcIilcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGFzeW5jIGZ1bmN0aW9uIGdldFByb2R1Y3QoaGFuZGxlKSB7XHJcbiAgICBjb25zdCBxdWVyeSA9IGBcclxuICAgIHtcclxuICAgICAgcHJvZHVjdChoYW5kbGU6IFwiJHtoYW5kbGV9XCIpIHtcclxuICAgICAgICBpZFxyXG4gICAgICAgIHZhcmlhbnRzKGZpcnN0OiAyNSkge1xyXG4gICAgICAgICAgZWRnZXMge1xyXG4gICAgICAgICAgICBub2RlIHtcclxuICAgICAgICAgICAgICBpZFxyXG4gICAgICAgICAgICAgIGF2YWlsYWJsZUZvclNhbGVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfWBcclxuXHJcbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IFNob3BpZnlEYXRhKHF1ZXJ5KVxyXG5cclxuICAgIGNvbnN0IHByb2R1Y3QgPSByZXNwb25zZS5kYXRhPy5wcm9kdWN0ID8gcmVzcG9uc2UuZGF0YS5wcm9kdWN0IDogW11cclxuICAgIGNvbnNvbGUubG9nKFwiUHJvZHVjdDogXCIsIHByb2R1Y3QpXHJcbiAgICByZXR1cm4gcHJvZHVjdFxyXG4gIH1cclxuXHJcbiAgY29uc3QgcHJvZHVjdCA9IGF3YWl0IGdldFByb2R1Y3QoaWQpXHJcbiAgcmVzLmpzb24ocHJvZHVjdClcclxufVxyXG4iXSwibmFtZXMiOlsic2VuZCIsInJlcSIsInJlcyIsInF1ZXJ5IiwiaWQiLCJkb21haW4iLCJwcm9jZXNzIiwiZW52IiwiU0hPUElGWV9TVE9SRV9ET01BSU4iLCJzdG9yZWZyb250QWNjZXNzVG9rZW4iLCJTSE9QSUZZX1NUT1JFRlJPTlRfQUNDRVNTVE9LRU4iLCJjb25zb2xlIiwibG9nIiwiU2hvcGlmeURhdGEiLCJVUkwiLCJvcHRpb25zIiwiZW5kcG9pbnQiLCJtZXRob2QiLCJoZWFkZXJzIiwiYm9keSIsIkpTT04iLCJzdHJpbmdpZnkiLCJkYXRhIiwiZmV0Y2giLCJ0aGVuIiwicmVzcG9uc2UiLCJqc29uIiwiZXJyb3IiLCJFcnJvciIsImdldFByb2R1Y3QiLCJoYW5kbGUiLCJwcm9kdWN0Il0sInNvdXJjZVJvb3QiOiIifQ==